# OnlineMall
基于springboot等技术的JavaWeb项目
包含前台和后台(从前台首页底部点击管理员登录)
数据库请按自己的情况导入，修改项目配置文件，导入sql文件到本地，然后运行项目即可进入首页。
![image](https://gitee.com/ljqzq/29DCH/blob/master/%E9%A6%96%E9%A1%B5.png)
# LJQ的电子商城

> 用到了gradle项目自动化构建工具开发项目，使用SpringBoot后台框架集成Spring-data-jpa,Druid连接池,thymeleaf(类似jsp)模板，
Bootstrap4，layui框架,Ajax异步交互实现的一个网上电子商城项目，包含后台管理和一系列操作，使用户
享受愉悦的购物体验！

项目预览网址：[http://localhost:8080/mall/](http://localhost:8080/mall/)

